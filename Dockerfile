FROM node:16.3.0-alpine

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install -g pm2
RUN npm ci

COPY . ./

RUN npm run build

EXPOSE 3000

CMD ["pm2-runtime", "--raw", "--no-auto-exit", "dist/main.js"]
