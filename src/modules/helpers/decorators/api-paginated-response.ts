import { applyDecorators, Type } from '@nestjs/common';
import { ApiOkResponse, getSchemaPath } from '@nestjs/swagger';

const ApiPaginatedResponse = <TModel extends Type<any>>(
  model: TModel,
) => applyDecorators(
    ApiOkResponse({
      description: 'OK',
      schema: {
        properties: {
          data: {
            type: 'array',
            items: { $ref: getSchemaPath(model) },
          },
          limit: {
            type: 'number',
          },
          page: {
            type: 'number',
          },
          total: {
            type: 'number',
          },
        },
      },
    }),
  );

export default ApiPaginatedResponse;
