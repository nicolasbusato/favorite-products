import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import UsersController from 'src/modules/users/users.controller';
import UsersService from 'src/modules/users/users.service';
import { User, UserSchema } from 'src/modules/users/schemas/users.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema },
    ]),
  ],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export default class UsersModule {}
