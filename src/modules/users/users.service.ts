import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { genSalt, hash } from 'bcrypt';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import CreateUserDto from 'src/modules/users/dto/create-user.dto';
import {
  User,
  UserDocument,
} from 'src/modules/users/schemas/users.schema';

@Injectable()
export default class UsersService {
  constructor(
    @InjectModel(User.name)
    private readonly UsersModel: Model<UserDocument>,
  ) {}

  async create(userDto: CreateUserDto): Promise<User> {
    try {
      const userInDB: any = await this.UsersModel.findOne({
        email: userDto.email,
        deleted_at: null,
      });

      if (userInDB) {
        throw new HttpException(
          `User (email: ${userDto.email}) already exists`,
          HttpStatus.BAD_REQUEST,
        );
      }

      const salt = await genSalt(10);
      const hashedPassword = await hash(userDto.password, salt);

      const user = await new this.UsersModel({
        email: userDto.email,
        password: hashedPassword,
      }).save();

      return user;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new HttpException(
        'Internal server error occurred on create a new user document',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async findByEmail(email: string): Promise<User> {
    try {
      const user = await this.UsersModel
        .findOne({
          email,
          deleted_at: null,
        });

      if (!user) {
        throw new HttpException(
          `User (email: ${email}) not found or not active!`,
          HttpStatus.NOT_FOUND,
        );
      }

      return user;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new HttpException(
        'Internal server error occurred on get the user document by email',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async findById(userID: Types.ObjectId): Promise<User> {
    try {
      const user = await this.UsersModel
        .findOne({
          where: {
            _id: userID,
            deleted_at: null,
          },
        });

      if (!user) {
        throw new HttpException(
          `User (ID: ${userID}) not found or not active!`,
          HttpStatus.NOT_FOUND,
        );
      }

      return user;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new HttpException(
        'Internal server error occurred on get the user document by id',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
