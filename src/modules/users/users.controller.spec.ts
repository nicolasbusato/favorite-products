import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import UsersService from 'src/modules/users/users.service';
import UsersController from 'src/modules/users/users.controller';
import { User } from 'src/modules/Users/schemas/users.schema';

describe('UsersController', () => {
  let controller: UsersController;
  let findOne: jest.Mock;

  beforeEach(async () => {
    findOne = jest.fn();

    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        UsersService,
        {
          provide: getModelToken(User.name),
          useValue: {
            findOne,
          },
        }],
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
