import {
  Controller,
  Post,
  Body,
  HttpCode,
} from '@nestjs/common';
import {
  ApiResponse,
  ApiTags,
  ApiBadRequestResponse,
  ApiBody,
} from '@nestjs/swagger';
import CreateUserDto from 'src/modules/users/dto/create-user.dto';
import UsersService from 'src/modules/users/users.service';

@ApiTags('Users')
@Controller('users')
export default class UsersController {
  constructor(private readonly usersService: UsersService) { }

  @Post()
  @HttpCode(201)
  @ApiBody({ type: CreateUserDto })
  @ApiResponse({ description: 'OK' })
  @ApiBadRequestResponse({ description: 'User already exist!' })
  create(@Body() createUserDto: CreateUserDto) {
    return this.usersService.create(createUserDto);
  }
}
