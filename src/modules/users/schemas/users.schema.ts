import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';
import Role from 'src/modules/users/enums/role.enum';

export type UserDocument = User & Document;

@Schema({
  collection: 'user',
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
  versionKey: false,
})
export class User {
  @Prop({ type: MongooseSchema.Types.ObjectId, auto: true })
    _id: string;

  @Prop({
    type: String,
    required: true,
  })
    email: string;

  @Prop({
    type: String,
    unique: true,
    required: true,
  })
    password: string;

  @Prop({
    type: [String],
    enum: Role,
    required: true,
    default: Role.standard,
  })
    roles: Role[];

  @Prop({ required: false })
    deleted_at?: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);
