import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { Types } from 'mongoose';
import UsersService from 'src/modules/users/users.service';
import { User } from 'src/modules/users/schemas/users.schema';

describe('UsersService', () => {
  let service: UsersService;
  let findOne: jest.Mock;

  beforeEach(async () => {
    findOne = jest.fn();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getModelToken(User.name),
          useValue: {
            findOne,
          },
        },
      ],
    }).compile();

    service = module.get<UsersService>(UsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('when creating a user by email and password', () => {
    const mockAuthUserDto = {
      email: 'test@test.com',
      password: 'mysupersecretpassword',
    };

    describe('and the user does exist', () => {
      beforeEach(() => {
        findOne.mockReturnValue(mockAuthUserDto);
      });

      it('should throw an error', async () => {
        await expect(service.create(mockAuthUserDto)).rejects.toThrow(
          `User (email: ${mockAuthUserDto.email}) already exists`,
        );
      });
    });
  });

  describe('when finding by email', () => {
    const mockAuthUserDto = {
      email: 'test@test.com',
      password: 'mysupersecretpassword',
    };

    describe('and the email doesnt exist', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
      });

      it('should throw an error', async () => {
        await expect(service.findByEmail(mockAuthUserDto.email)).rejects.toThrow(
          `User (email: ${mockAuthUserDto.email}) not found or not active!`,
        );
      });
    });
  });

  describe('when finding by id', () => {
    describe('and the id doesnt exist', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
      });

      it('should throw an error', async () => {
        const objectId = new Types.ObjectId();
        await expect(service.findById(objectId)).rejects.toThrow(
          `User (ID: ${objectId}) not found or not active!`,
        );
      });
    });
  });
});
