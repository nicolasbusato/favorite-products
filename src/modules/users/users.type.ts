import { User } from 'src/modules/users/schemas/users.schema';

export type IUsers = Omit<User, 'password'>;
