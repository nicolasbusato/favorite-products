import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import * as Joi from 'joi';
import envsConfig from 'src/config/envs.config';
import AppController from 'src/modules/app.controller';
import DatabaseModule from 'src/modules/database/database.module';
import HealthModule from 'src/modules/health/health.module';
import ClientModule from 'src/modules/client/client.module';
import ProductModule from 'src/modules/product/product.module';
import UsersModule from 'src/modules/users/users.module';
import AuthModule from 'src/modules/auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      validationSchema: Joi.object({
        NODE_ENV: Joi.string()
          .valid('development', 'production', 'test', 'provision')
          .default('development'),
        SERVICE_NAME: Joi.string().default('favorite-products'),
        SERVICE_PORT: Joi.number().default(3000),
        DB_MONGO_HOST: Joi.string().required().default('localhost'),
        DB_MONGO_PORT: Joi.string().required().default('27017'),
        DB_MONGO_USERNAME: Joi.string().required().default('root'),
        DB_MONGO_PASSWORD: Joi.string().required().default('pass12345'),
        DB_MONGO_DATABASE: Joi.string().required().default('database'),
        JWT_SECRET: Joi.string().default('mysupersecret'),
        JWT_SECRET_EXPIRES: Joi.string().default(10000),
        PRODUCTS_ENDPOINT: Joi.string().default('http://localhost:3001'),
      }),
      validationOptions: {
        allowUnknown: true,
        abortEarly: true,
      },
      load: [envsConfig],
      isGlobal: true,
    }),
    DatabaseModule,
    HealthModule,
    ClientModule,
    ProductModule,
    UsersModule,
    AuthModule,
  ],
  controllers: [AppController],
})
export default class AppModule {}
