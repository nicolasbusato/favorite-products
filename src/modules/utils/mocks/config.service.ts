const mockedConfigService = {
  get(key: string) {
    switch (key) {
      case 'jwt.secret':
        return 'super secret';
      default:
        return 'favorite-products';
    }
  },
};

export default mockedConfigService;
