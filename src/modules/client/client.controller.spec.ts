import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import ClientService from 'src/modules/client/client.service';
import ClientController from 'src/modules/client/client.controller';
import { Client } from 'src/modules/client/schemas/client.schema';
import ProductService from 'src/modules/product/product.service';
import { FavoriteProduct } from 'src/modules/client/schemas/favorite-products.schema';
import { ConfigService } from '@nestjs/config';
import { HttpService } from '@nestjs/axios';

describe('ClientController', () => {
  let controller: ClientController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClientController],
      providers: [
        ConfigService,
        ClientService,
        ProductService,
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: getModelToken(Client.name),
          useValue: {},
        },
        {
          provide: getModelToken(FavoriteProduct.name),
          useValue: {},
        },
      ],
    }).compile();

    controller = module.get<ClientController>(ClientController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
