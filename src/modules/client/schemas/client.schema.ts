import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';

export type ClientDocument = Client & Document;

@Schema({
  collection: 'client',
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
  versionKey: false,
})
export class Client {
  @Prop({ type: MongooseSchema.Types.ObjectId, auto: true })
    _id: string;

  @Prop({
    type: String,
    required: true,
  })
    name: string;

  @Prop({
    type: String,
    required: true,
  })
    email: string;

  @Prop({ required: false })
    deleted_at?: Date;
}

export const ClientSchema = SchemaFactory.createForClass(Client);
