import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Schema as MongooseSchema } from 'mongoose';

export type FavoriteProductDocument = FavoriteProduct & Document;

@Schema({
  collection: 'favorite_product',
  timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at',
  },
  versionKey: false,
})
export class FavoriteProduct {
  @Prop({
    type: MongooseSchema.Types.ObjectId,
    auto: true,
  })
    _id: string;

  @Prop({
    type: String,
    required: true,
  })
    external_id: string;

  @Prop({
    type: MongooseSchema.Types.ObjectId,
    required: true,
  })
    client_id: {
    type: MongooseSchema.Types.ObjectId,
    ref: 'Client'
  };

  @Prop({
    required: true,
  })
    price: number;

  @Prop({
    required: true,
  })
    image: string;

  @Prop({
    required: true,
  })
    brand: string;

  @Prop({
    required: true,
  })
    title: string;

  @Prop({
    required: false,
  })
    reviewScore: number;

  @Prop({
    required: false,
  })
    deleted_at?: Date;
}

export const FavoriteProductSchema = SchemaFactory.createForClass(FavoriteProduct);
