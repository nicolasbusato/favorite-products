import {
  Body,
  Controller,
  HttpCode,
  Post,
  Get,
  Query,
  Delete,
  Param,
  Patch,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiBody,
  ApiResponse,
  ApiTags,
  ApiNotFoundResponse,
  ApiUnauthorizedResponse,
  ApiNoContentResponse,
} from '@nestjs/swagger';
import { Types } from 'mongoose';
import AuthJwtGuard from 'src/modules/auth/auth-jwt.guard';
import ClientDto from 'src/modules/client/dto/client.dto';
import CreateClientDto from 'src/modules/client/dto/create-client.dto';
import ClientService from 'src/modules/client/client.service';
import ApiPaginatedResponse from 'src/modules/helpers/decorators/api-paginated-response';
import QueryClientDto from 'src/modules/client/dto/query-client.dto';
import PatchClientDto from 'src/modules/client/dto/patch-client.dto';
import FavoriteProductDto from 'src/modules/client/dto/favorite-products.dto';
import BodyFavoriteProductDto from 'src/modules/client/dto/body-favorite-products.dto';

@Controller('clients')
@ApiTags('Clients')
export default class ClientController {
  constructor(private readonly clientService: ClientService) {}

  @Get()
  @UseGuards(AuthJwtGuard)
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiPaginatedResponse(ClientDto)
  async getClients(@Query() queryFilter: QueryClientDto) {
    return this.clientService.getClients(queryFilter);
  }

  @Post()
  @UseGuards(AuthJwtGuard)
  @HttpCode(201)
  @ApiBody({ type: CreateClientDto })
  @ApiResponse({ description: 'OK' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiBadRequestResponse({ description: 'Client already exist!' })
  @ApiResponse({ type: ClientDto })
  async create(@Body() clientDto: CreateClientDto) {
    return this.clientService.create(clientDto);
  }

  @Patch(':clientID')
  @UseGuards(AuthJwtGuard)
  @HttpCode(204)
  @ApiBody({ type: PatchClientDto })
  @ApiNoContentResponse({ description: 'OK' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiNotFoundResponse({ description: 'Client not found or deleted!' })
  @ApiBadRequestResponse({ description: 'Client with same email found!' })
  async updateByID(
  @Param('clientID') clientID: Types.ObjectId,
    @Body() patchClientDto: PatchClientDto,
  ) {
    return this.clientService.updateByID(
      clientID,
      patchClientDto,
    );
  }

  @Get(':clientID')
  @UseGuards(AuthJwtGuard)
  @ApiNotFoundResponse({ description: 'Client not found' })
  @ApiResponse({ type: ClientDto })
  async getByID(
  @Param('clientID') clientID: Types.ObjectId,
  ) {
    return this.clientService.getByID(clientID);
  }

  @Delete(':clientID')
  @UseGuards(AuthJwtGuard)
  @HttpCode(204)
  @ApiNoContentResponse({ description: 'OK' })
  @ApiNotFoundResponse({ description: 'Client not found' })
  async deleteByID(
  @Param('clientID') clientID: Types.ObjectId,
  ) {
    return this.clientService.deleteByID(clientID);
  }

  @Get(':clientID/favorite-products')
  @UseGuards(AuthJwtGuard)
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiNotFoundResponse({ description: 'Client not found' })
  @ApiPaginatedResponse(FavoriteProductDto)
  async getFavoriteProductsByClientId(
  @Param('clientID') clientID: Types.ObjectId,
    @Query() queryFilter: QueryClientDto,
  ) {
    return this.clientService.getFavoriteProductsByClientId(clientID, queryFilter);
  }

  @Post(':clientID/favorite-products')
  @UseGuards(AuthJwtGuard)
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiBadRequestResponse({ description: 'Favorite Products already exist in DB!' })
  @ApiNotFoundResponse({ description: 'Client not found' })
  async createFavoriteProductsByClientID(
  @Param('clientID') clientID: Types.ObjectId,
    @Body() body: BodyFavoriteProductDto,
  ) {
    return this.clientService.createFavoriteProductsByClientID(clientID, body);
  }

  @Delete(':clientID/favorite-products')
  @UseGuards(AuthJwtGuard)
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiNotFoundResponse({ description: 'Client not found' })
  async deleteFavoriteProductsByClientID(
  @Param('clientID') clientID: Types.ObjectId,
    @Body() body: BodyFavoriteProductDto,
  ) {
    return this.clientService.deleteFavoriteProductsByClientID(clientID, body);
  }
}
