import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import ClientService from 'src/modules/client/client.service';
import ProductService from 'src/modules/product/product.service';
import { Client } from 'src/modules/client/schemas/client.schema';
import { FavoriteProduct } from 'src/modules/client/schemas/favorite-products.schema';
import { ConfigService } from '@nestjs/config';
import { HttpService } from '@nestjs/axios';
import { Types } from 'mongoose';

describe('ClientService', () => {
  let service: ClientService;
  let findOne: jest.Mock;
  let find: jest.Mock;

  beforeEach(async () => {
    findOne = jest.fn();
    find = jest.fn();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ConfigService,
        ClientService,
        ProductService,
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: getModelToken(Client.name),
          useValue: { findOne, find },
        },
        {
          provide: getModelToken(FavoriteProduct.name),
          useValue: { findOne, find },
        },
      ],
    }).compile();

    service = module.get<ClientService>(ClientService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('when get client by id', () => {
    const objectId = new Types.ObjectId();

    describe('and the user doesnt exist', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
      });

      it('should throw an error', async () => {
        await expect(service.getByID(objectId)).rejects.toThrow(
          `Client (ID: ${objectId}) not found`,
        );
      });
    });
  });

  describe('when updating client by id', () => {
    const mockClientDto = {
      email: 'test@test.com',
      name: 'thisismyname',
    };
    const objectId = new Types.ObjectId();

    describe('and the user doesnt exist', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
      });

      it('should throw an error', async () => {
        await expect(service.updateByID(objectId, mockClientDto)).rejects.toThrow(
          `Client (ID: ${objectId}) not found or deleted!`,
        );
      });
    });

    describe('and the email aready exist', () => {
      beforeEach(() => {
        findOne.mockReturnValue(mockClientDto);
      });

      it('should throw an error', async () => {
        await expect(service.updateByID(objectId, mockClientDto)).rejects.toThrow(
          `Client (email: ${mockClientDto.email}) with same email found!`,
        );
      });
    });
  });

  describe('when creating client', () => {
    const mockClientDto = {
      email: 'test@test.com',
      name: 'thisismyname',
    };

    describe('and the user email already exist', () => {
      beforeEach(() => {
        findOne.mockReturnValue(mockClientDto);
      });

      it('should throw an error', async () => {
        await expect(service.create(mockClientDto)).rejects.toThrow(
          `Client (email: ${mockClientDto.email}) already exist!`,
        );
      });
    });
  });

  describe('when getting client favorite products', () => {
    const mockQueryFilterDto = {
      page: '1',
      limit: '10',
    };
    const objectId = new Types.ObjectId();

    describe('and the client doesnt exist', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
      });

      it('should throw an error', async () => {
        await expect(service.getFavoriteProductsByClientId(objectId, mockQueryFilterDto))
          .rejects.toThrow(
            `Client (ID: ${objectId}) not found or deleted!`,
          );
      });
    });
  });

  describe('when creating client favorite products', () => {
    const mockClientDto = {
      name: 'thisismyname',
      email: 'test@test.com',
    };
    const mockBodyFavoriteProductsDto = {
      product_ids: ['1', '2'],
    };
    const objectId = new Types.ObjectId();

    describe('and the client doesnt exist', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
      });

      it('should throw an error', async () => {
        await expect(service.createFavoriteProductsByClientID(
          objectId,
          mockBodyFavoriteProductsDto,
        )).rejects.toThrow(
          `Client (ID: ${objectId}) not found or deleted!`,
        );
      });
    });

    describe('and the client already has this favorite products', () => {
      beforeEach(() => {
        findOne.mockReturnValue(mockClientDto);
        find.mockReturnValue(['1', '2']);
      });

      it('should throw an error', async () => {
        await expect(service.createFavoriteProductsByClientID(
          objectId,
          mockBodyFavoriteProductsDto,
        )).rejects.toThrow(
          'Favorite Products already exist in DB!',
        );
      });
    });
  });

  describe('when deleting client favorite products', () => {
    const mockClientDto = {
      name: 'thisismyname',
      email: 'test@test.com',
    };
    const mockBodyFavoriteProductsDto = {
      product_ids: ['1', '2'],
    };
    const objectId = new Types.ObjectId();

    describe('and the client doesnt exist', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
      });

      it('should throw an error', async () => {
        await expect(service.deleteFavoriteProductsByClientID(
          objectId,
          mockBodyFavoriteProductsDto,
        )).rejects.toThrow(
          `Client (ID: ${objectId}) not found or deleted!`,
        );
      });
    });

    describe('and the client doesnt have this products', () => {
      beforeEach(() => {
        findOne.mockReturnValue(mockClientDto);
        find.mockReturnValue(['1']);
      });

      it('should throw an error', async () => {
        await expect(service.deleteFavoriteProductsByClientID(
          objectId,
          mockBodyFavoriteProductsDto,
        )).rejects.toThrow(
          'Favorite Products not found in DB!',
        );
      });
    });
  });
});
