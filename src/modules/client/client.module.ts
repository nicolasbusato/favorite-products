import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import ClientController from 'src/modules/client/client.controller';
import ClientService from 'src/modules/client/client.service';
import { Client, ClientSchema } from 'src/modules/client/schemas/client.schema';
import { FavoriteProduct, FavoriteProductSchema } from 'src/modules/client/schemas/favorite-products.schema';
import ProductModule from 'src/modules/product/product.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Client.name, schema: ClientSchema },
      { name: FavoriteProduct.name, schema: FavoriteProductSchema },
    ]),
    ProductModule,
  ],
  controllers: [ClientController],
  providers: [ClientService],
  exports: [ClientService],
})
export default class ClientModule {}
