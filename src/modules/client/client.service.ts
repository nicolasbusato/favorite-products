import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import CreateClientDto from 'src/modules/client/dto/create-client.dto';
import QueryClientDto from 'src/modules/client/dto/query-client.dto';
import PatchClientDto from 'src/modules/client/dto/patch-client.dto';
import BodyFavoriteProductDto from 'src/modules/client/dto/body-favorite-products.dto';
import {
  Client,
  ClientDocument,
} from 'src/modules/client/schemas/client.schema';
import {
  FavoriteProduct,
  FavoriteProductDocument,
} from 'src/modules/client/schemas/favorite-products.schema';
import ProductService from 'src/modules/product/product.service';
import { lastValueFrom } from 'rxjs';

@Injectable()
export default class ClientService {
  constructor(
    @InjectModel(Client.name)
    private readonly ClientModel: Model<ClientDocument>,
    @InjectModel(FavoriteProduct.name)
    private readonly FavoriteProductModel: Model<FavoriteProductDocument>,
    private readonly productService: ProductService,
  ) { }

  async getByID(clientID: Types.ObjectId): Promise<Client> {
    try {
      const client = await this.ClientModel
        .findOne({ _id: clientID, deleted_at: null });

      if (!client) {
        throw new HttpException(
          `Client (ID: ${clientID}) not found`,
          HttpStatus.NOT_FOUND,
        );
      }

      return client;
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new HttpException(
        `Internal server error occurred on get client document by ID (${clientID})`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async getClients(queryFilter: QueryClientDto) {
    try {
      const page = queryFilter.page ? parseInt(queryFilter.page, 10) : 1;
      const limit = queryFilter.limit ? parseInt(queryFilter.limit, 10) : 10;

      const filterClients = { deleted_at: null };

      const clientsPromise = this.ClientModel
        .find(filterClients)
        .skip((page - 1) * limit)
        .limit(limit);

      const countPromise = this.ClientModel
        .find(filterClients)
        .countDocuments();

      const [data, total] = await Promise.all([
        clientsPromise,
        countPromise,
      ]);

      return {
        data, limit, page, total,
      };
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new HttpException(
        'Internal server error occurred on get clients documents',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async updateByID(
    clientID: Types.ObjectId,
    patchClientDto: PatchClientDto,
  ) {
    try {
      const clientInDb = await this.ClientModel
        .findOne({ _id: clientID, deleted_at: null });

      if (!clientInDb) {
        throw new HttpException(
          `Client (ID: ${clientID}) not found or deleted!`,
          HttpStatus.NOT_FOUND,
        );
      }

      if (patchClientDto.email) {
        const clientWithSameEmail = await this.ClientModel
          .findOne({ email: patchClientDto.email, deleted_at: null });

        if (clientWithSameEmail) {
          throw new HttpException(
            `Client (email: ${patchClientDto.email}) with same email found!`,
            HttpStatus.BAD_REQUEST,
          );
        }
      }

      await this.ClientModel
        .updateOne({ _id: clientID }, patchClientDto);
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new HttpException(
        'Internal server error occurred on updating the client document',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async deleteByID(clientID: Types.ObjectId) {
    try {
      await this.ClientModel.findByIdAndUpdate(clientID, { deleted_at: new Date(Date.now()) });
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new HttpException(
        'Internal server error occurred on deleting the client document',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async create(createClientDto: CreateClientDto): Promise<Client> {
    try {
      const clientInDb = await this.ClientModel
        .findOne({ email: createClientDto.email, deleted_at: null });

      if (clientInDb) {
        throw new HttpException(
          `Client (email: ${createClientDto.email}) already exist!`,
          HttpStatus.BAD_REQUEST,
        );
      }

      return await new this.ClientModel(createClientDto).save();
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new HttpException(
        'Internal server error occurred on create a new client document',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async getFavoriteProductsByClientId(clientID: Types.ObjectId, queryFilter: QueryClientDto) {
    try {
      const client = await this.ClientModel
        .findOne({ _id: clientID, deleted_at: null });

      if (!client) {
        throw new HttpException(
          `Client (ID: ${clientID}) not found or deleted!`,
          HttpStatus.NOT_FOUND,
        );
      }

      const page = queryFilter.page ? parseInt(queryFilter.page, 10) : 1;
      const limit = queryFilter.limit ? parseInt(queryFilter.limit, 10) : 10;

      const filterFavoriteProducts = { client_id: clientID, deleted_at: null };

      const favoriteProductsPromise = this.FavoriteProductModel
        .find(filterFavoriteProducts)
        .skip((page - 1) * limit)
        .limit(limit);

      const countPromise = this.FavoriteProductModel
        .find(filterFavoriteProducts)
        .countDocuments();

      const [data, total] = await Promise.all([
        favoriteProductsPromise,
        countPromise,
      ]);

      return {
        data, limit, page, total,
      };
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new HttpException(
        'Internal server error occurred on get favorite products documents',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async createFavoriteProductsByClientID(
    clientID: Types.ObjectId,
    bodyFavoriteProductDto: BodyFavoriteProductDto,
  ) {
    try {
      const client = await this.ClientModel
        .findOne({ _id: clientID, deleted_at: null });

      if (!client) {
        throw new HttpException(
          `Client (ID: ${clientID}) not found or deleted!`,
          HttpStatus.NOT_FOUND,
        );
      }
      const favoriteProductIDs = bodyFavoriteProductDto.product_ids;

      const favoriteProductsInDB = await this.FavoriteProductModel
        .find({
          client_id: clientID,
          external_id: { $in: favoriteProductIDs },
          deleted_at: null,
        });

      if (favoriteProductsInDB.length > 0) {
        throw new HttpException(
          'Favorite Products already exist in DB!',
          HttpStatus.BAD_REQUEST,
        );
      }

      const productsPromises = favoriteProductIDs.map(
        (productId: string) => lastValueFrom(this.productService.getByID(productId)),
      );
      const products = await Promise.all(productsPromises);

      if (products.length !== favoriteProductIDs.length) {
        throw new HttpException(
          'One or more products were not found! See if the product id\'s are correct',
          HttpStatus.BAD_REQUEST,
        );
      }

      const favoriteProducts = products.map((product: any) => {
        const favoriteProduct: any = {
          price: product.price,
          external_id: product.id,
          client_id: clientID,
          image: product.image,
          brand: product.brand,
          title: product.title,
        };
        if (product.reviewScore) {
          favoriteProduct.reviewScore = product.reviewScore;
        }
        return favoriteProduct;
      });

      await this.FavoriteProductModel.insertMany(favoriteProducts);
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new HttpException(
        'Internal server error occurred on updating the favorite products document',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  async deleteFavoriteProductsByClientID(
    clientID: Types.ObjectId,
    bodyFavoriteProductDto: BodyFavoriteProductDto,
  ) {
    try {
      const client = await this.ClientModel
        .findOne({ _id: clientID, deleted_at: null });

      if (!client) {
        throw new HttpException(
          `Client (ID: ${clientID}) not found or deleted!`,
          HttpStatus.NOT_FOUND,
        );
      }

      const favoriteProductIDs = bodyFavoriteProductDto.product_ids;

      const favoriteProductsInDB = await this.FavoriteProductModel
        .find({
          client_id: clientID,
          external_id: { $in: favoriteProductIDs },
          deleted_at: null,
        });

      if (favoriteProductsInDB.length !== favoriteProductIDs.length) {
        throw new HttpException(
          'Favorite Products not found in DB!',
          HttpStatus.BAD_REQUEST,
        );
      }

      await this.FavoriteProductModel.updateMany(
        {
          external_id: { $in: favoriteProductIDs },
        },
        {
          deleted_at: new Date(Date.now()),
        },
      );
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new HttpException(
        'Internal server error occurred on deleting the favorite products document',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
