import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsOptional,
} from 'class-validator';

export default class QueryClientDto {
  @ApiProperty()
  @IsString()
  @IsOptional()
    page: string;

  @ApiProperty()
  @IsString()
  @IsOptional()
    limit: string;
}
