import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsString,
  IsOptional,
  IsNumber,
} from 'class-validator';

export default class FavoriteProductDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
    _id: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
    price: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
    external_id: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
    client_id: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
    image: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
    brand: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
    title: string;

  @ApiProperty()
  @IsNumber()
  @IsOptional()
    reviewScore: number;
}
