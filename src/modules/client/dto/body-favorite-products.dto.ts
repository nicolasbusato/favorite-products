import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsArray,
} from 'class-validator';

export default class BodyFavoriteProductDto {
  @ApiProperty()
  @IsArray()
  @IsNotEmpty()
    product_ids: Array<string>;
}
