import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsString,
  IsEmail,
} from 'class-validator';

export default class ClientDto {
  @ApiProperty()
    _id: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
    name: string;

  @ApiProperty()
  @IsEmail()
  @IsNotEmpty()
    email: string;

  @ApiProperty()
    created_at: string;

  @ApiProperty()
    updated_at: string;

  deleted_at: string;
}
