import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsEmail,
  IsOptional,
} from 'class-validator';

export default class PatchClientDto {
  @ApiProperty()
  @IsString()
  @IsOptional()
    name: string;

  @ApiProperty()
  @IsEmail()
  @IsOptional()
    email: string;
}
