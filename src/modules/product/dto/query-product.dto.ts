import { ApiProperty } from '@nestjs/swagger';
import {
  IsOptional,
  IsString,
} from 'class-validator';

export default class QueryProductDTO {
  @ApiProperty()
  @IsString()
  @IsOptional()
    page: string;
}
