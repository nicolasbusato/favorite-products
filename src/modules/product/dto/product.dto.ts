import { ApiProperty } from '@nestjs/swagger';
import {
  IsNotEmpty,
  IsString,
  IsOptional,
  IsNumber,
} from 'class-validator';

export default class ProductDTO {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
    id: string;

  @ApiProperty()
  @IsNumber()
  @IsNotEmpty()
    price: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
    image: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
    brand: string;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
    title: string;

  @ApiProperty()
  @IsNumber()
  @IsOptional()
    reviewScore: number;
}
