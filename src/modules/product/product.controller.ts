import {
  Controller,
  Get,
  Query,
  Param,
} from '@nestjs/common';
import {
  ApiResponse,
  ApiTags,
  ApiBody,
} from '@nestjs/swagger';
import ProductDTO from 'src/modules/product/dto/product.dto';
import QueryProductDTO from 'src/modules/product/dto/query-product.dto';
import ProductService from 'src/modules/product/product.service';

@Controller('products')
@ApiTags('Products')
export default class ProductController {
  constructor(private readonly productService: ProductService) {}

  @Get()
  @ApiBody({ type: QueryProductDTO })
  @ApiResponse({ type: [ProductDTO] })
  getProducts(@Query() queryFilter: QueryProductDTO) {
    return this.productService.getProducts(queryFilter);
  }

  @Get(':productID')
  @ApiResponse({ type: ProductDTO })
  getByID(@Param('productID') productID: string) {
    return this.productService.getByID(productID);
  }
}
