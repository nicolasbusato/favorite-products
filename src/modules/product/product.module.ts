import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import ProductController from 'src/modules/product/product.controller';
import ProductService from 'src/modules/product/product.service';

@Module({
  imports: [HttpModule],
  controllers: [ProductController],
  providers: [ProductService],
  exports: [ProductService],
})
export default class ProductModule {}
