import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import ProductService from 'src/modules/product/product.service';
import { HttpService } from '@nestjs/axios';

describe('ProductService', () => {
  let service: ProductService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProductService,
        ConfigService,
        {
          provide: HttpService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<ProductService>(ProductService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
