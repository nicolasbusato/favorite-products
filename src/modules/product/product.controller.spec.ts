import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import ProductController from 'src/modules/product/product.controller';
import ProductService from 'src/modules/product/product.service';
import { HttpService } from '@nestjs/axios';

describe('ProductController', () => {
  let controller: ProductController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductController],
      providers: [
        ProductService,
        ConfigService,
        {
          provide: HttpService,
          useValue: {},
        },
      ],
    }).compile();

    controller = module.get<ProductController>(ProductController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
