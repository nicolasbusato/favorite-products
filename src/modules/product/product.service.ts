import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import ProductDTO from 'src/modules/product/dto/product.dto';
import QueryProductDTO from 'src/modules/product/dto/query-product.dto';

@Injectable()
export default class ProductService {
  private readonly EXTERNAL_API_URL = `${this.configService.get('services.products')}/`;

  constructor(
    private readonly configService: ConfigService,
    private httpService: HttpService,
  ) {}

  getByID(productID: string): Observable<AxiosResponse<ProductDTO>> {
    try {
      return this.httpService
        .get(`${this.EXTERNAL_API_URL}${productID}/`)
        .pipe(map((axiosResponse: AxiosResponse) => axiosResponse.data));
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new HttpException(
        `Internal server error occurred on get by ID (${productID}) from external API`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  getProducts(queryFilter: QueryProductDTO): Observable<AxiosResponse<ProductDTO[]>> {
    try {
      const page = queryFilter.page ? parseInt(queryFilter.page, 10) : 1;

      return this.httpService
        .get(`${this.EXTERNAL_API_URL}?page=${page}`)
        .pipe(map((axiosResponse: AxiosResponse) => axiosResponse.data));
    } catch (err) {
      if (err instanceof HttpException) {
        throw err;
      }

      throw new HttpException(
        'Internal server error occurred on get products from external API',
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }
}
