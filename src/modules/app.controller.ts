import { Controller, Get, Redirect } from '@nestjs/common';
import { ApiExcludeEndpoint } from '@nestjs/swagger';

@Controller()
export default class AppController {
  // eslint-disable-next-line class-methods-use-this
  @Get()
  @Redirect('health')
  @ApiExcludeEndpoint()
  redirect() { }
}
