import { INestApplication, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';
import { User } from 'src/modules/users/schemas/users.schema';
import UsersService from 'src/modules/users/users.service';
import mockedConfigService from 'src/modules/utils/mocks/config.service';
import mockedJwtService from 'src/modules/utils/mocks/jwt.service';
import * as request from 'supertest';
import AuthJwtStrategy from 'src/modules/auth/auth-jwt.strategy';
import AuthLocalStrategy from 'src/modules/auth/auth-local.strategy';
import AuthController from 'src/modules/auth/auth.controller';
import AuthService from 'src/modules/auth/auth.service';

describe('AuthController', () => {
  let app: INestApplication;
  let findOne: jest.Mock;

  beforeEach(async () => {
    findOne = jest.fn();

    const module: TestingModule = await Test.createTestingModule({
      imports: [PassportModule],
      controllers: [AuthController],
      providers: [
        AuthService,
        UsersService,
        AuthLocalStrategy,
        AuthJwtStrategy,
        {
          provide: ConfigService,
          useValue: mockedConfigService,
        },
        {
          provide: JwtService,
          useValue: mockedJwtService,
        },
        {
          provide: getModelToken(User.name),
          useValue: {
            findOne,
          },
        },
      ],
    }).compile();

    app = module.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  describe('when login', () => {
    const mockAuthUserDto = {
      email: 'test@test.com',
      password: 'mysupersecretpassword',
    };

    describe('and using invalid data', () => {
      describe('missing email', () => {
        it('should throw an error', () => {
          const dto = {
            ...mockAuthUserDto,
          };

          delete dto.email;

          return request(app.getHttpServer())
            .post('/auth/login')
            .send(dto)
            .expect(401);
        });
      });

      describe('missing password', () => {
        it('should throw an error', () => {
          const dto = {
            ...mockAuthUserDto,
          };

          delete dto.password;

          return request(app.getHttpServer())
            .post('/auth/login')
            .send(dto)
            .expect(401);
        });
      });
    });

    describe('and using valid data', () => {
      describe('and user not exists', () => {
        beforeEach(() => {
          findOne.mockReturnValue(undefined);
        });

        it('should throw an error', () => request(app.getHttpServer())
          .post('/auth/login')
          .send(mockAuthUserDto)
          .expect(404));
      });

      describe('and user exists', () => {
        describe('and password does not match', () => {
          let user: User;

          beforeEach(() => {
            user = new User();

            user.email = mockAuthUserDto.email;
            user.password = bcrypt.hashSync('mynewsupersecretpassword', 10);

            findOne.mockReturnValue(Promise.resolve(user));
          });

          it('should throw an error', () => request(app.getHttpServer())
            .post('/auth/login')
            .send(mockAuthUserDto)
            .expect(422));
        });
      });
    });
  });

  afterAll(async () => {
    await app.close();
  });
});
