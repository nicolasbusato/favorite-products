import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { IUsers } from 'src/modules/users/users.type';

const AuthUsers = createParamDecorator(
  (data: string, ctx: ExecutionContext): IUsers => {
    const req = ctx.switchToHttp().getRequest();
    return data ? req.user?.[data] : req.user;
  },
);

export default AuthUsers;
