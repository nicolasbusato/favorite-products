import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';
import { User } from 'src/modules/users/schemas/users.schema';
import UsersService from 'src/modules/users/users.service';
import mockedConfigService from 'src/modules/utils/mocks/config.service';
import mockedJwtService from 'src/modules/utils/mocks/jwt.service';
import AuthService from 'src/modules/auth/auth.service';

describe('AuthService', () => {
  let service: AuthService;
  let findOne: jest.Mock;

  beforeEach(async () => {
    findOne = jest.fn();

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        UsersService,
        {
          provide: ConfigService,
          useValue: mockedConfigService,
        },
        {
          provide: JwtService,
          useValue: mockedJwtService,
        },
        {
          provide: getModelToken(User.name),
          useValue: {
            findOne,
          },
        },
      ],
    }).compile();

    service = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('when validate a user by email and password', () => {
    const mockAuthUserDto = {
      email: 'test@test.com',
      password: 'mysupersecretpassword',
    };

    describe('and the user does not exists', () => {
      beforeEach(() => {
        findOne.mockReturnValue(undefined);
      });

      it('should throw an error', async () => {
        await expect(service.validateUser(mockAuthUserDto)).rejects.toThrow(
          `User (email: ${mockAuthUserDto.email}) not found or not active!`,
        );
      });
    });

    describe('and the password is not matched', () => {
      let user: User;

      beforeEach(() => {
        user = new User();

        user.email = mockAuthUserDto.email;
        user.password = bcrypt.hashSync('mynewsupersecretpassword', 10);

        findOne.mockReturnValue(Promise.resolve(user));
      });

      it('should throw an error', async () => {
        await expect(service.validateUser(mockAuthUserDto)).rejects.toThrow(
          'Invald credentials',
        );
      });
    });
  });
});
