import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import UsersService from 'src/modules/users/users.service';
import { IUsers } from 'src/modules/users/users.type';
import AuthUsersDto from 'src/modules/auth/dto/auth-users.dto';

@Injectable()
export default class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(authUsersDto: AuthUsersDto): Promise<IUsers> {
    const user = await this.usersService.findByEmail(authUsersDto.email);

    const isPasswordMatching = await bcrypt.compare(authUsersDto.password, user.password);

    if (!isPasswordMatching) {
      throw new HttpException(
        'Invald credentials',
        HttpStatus.UNPROCESSABLE_ENTITY,
      );
    }

    return user;
  }

  login(user: IUsers) {
    return {
      access_token: this.jwtService.sign(
        {
          email: user.email,
          permissions: user.roles,
        },
        {
          // eslint-disable-next-line no-underscore-dangle
          subject: String(user._id),
          issuer: this.configService.get('service.name'),
        },
      ),
    };
  }
}
