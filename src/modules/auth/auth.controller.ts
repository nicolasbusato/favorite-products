import { Controller, Post, UseGuards } from '@nestjs/common';
import {
  ApiBody,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiTags,
  ApiUnauthorizedResponse,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import { IUsers } from 'src/modules/users/users.type';
import AuthLocalGuard from 'src/modules/auth/auth-local.guard';
import AuthUsers from 'src/modules/auth/auth-users.decorator';
import AuthService from 'src/modules/auth/auth.service';
import AuthUsersDto from 'src/modules/auth/dto/auth-users.dto';

@ApiTags('Authentication')
@Controller('auth')
@ApiInternalServerErrorResponse({
  description: 'Erro interno do servidor',
})
export default class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(AuthLocalGuard)
  @Post('login')
  @ApiUnprocessableEntityResponse({ description: 'Invalid Credentials' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiNotFoundResponse({ description: 'Not Found' })
  @ApiCreatedResponse({
    description: 'User authenticated',
    schema: {
      type: 'object',
      properties: {
        access_token: {
          type: 'string',
          description: 'Token de acesso',
        },
      },
    },
  })
  @ApiBody({ type: AuthUsersDto })
  login(@AuthUsers() users: IUsers) {
    return this.authService.login(users);
  }
}
