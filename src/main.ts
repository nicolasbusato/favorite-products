import { ValidationPipe, NestApplicationOptions } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import helmet from 'helmet';
import { WinstonModule } from 'nest-winston';
import AppModule from 'src/modules/app.module';
import AppLogging from 'src/modules/app.logging';

async function bootstrap() {
  const nestAppOptions : NestApplicationOptions = {
    logger: WinstonModule.createLogger(AppLogging),
  };

  const app = await NestFactory.create(AppModule, nestAppOptions);

  const configService = app.get(ConfigService);

  app.enableCors();
  app.use(helmet());
  app.useGlobalPipes(new ValidationPipe({ transform: true }));

  const APP_VERSION = process.env.npm_package_version;
  const APP_NAME = process.env.npm_package_name;

  const options = new DocumentBuilder()
    .setTitle(APP_NAME)
    .setDescription('Endpoints List')
    .setVersion(APP_VERSION)
    .addBearerAuth()
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('/api', app, document, {
    swaggerOptions: {
      persistAuthorization: true,
    },
  });

  const port = configService.get<number>('service.port');

  app.listen(port);
}

bootstrap();
