export default () => ({
  service: {
    name: process.env.SERVICE_NAME,
    port: process.env.SERVICE_PORT,
  },
  env: process.env.NODE_ENV,
  database: {
    host: process.env.DB_MONGO_HOST,
    port: process.env.DB_MONGO_PORT,
    username: process.env.DB_MONGO_USERNAME,
    password: process.env.DB_MONGO_PASSWORD,
    name: process.env.DB_MONGO_DATABASE,
  },
  jwt: {
    secret: process.env.JWT_SECRET,
    expiresIn: process.env.JWT_SECRET_EXPIRES || 10000,
  },
  services: {
    products: process.env.PRODUCTS_ENDPOINT || '',
  },
});
