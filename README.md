# Favorite Products

## Descrição

Sistema de gerenciamento de produtos favoritos dos clientes.

Fluxo:

- Criação de usuário (Users).
- Autenticação desse usuário retornando uma JWT.
- CRUD dos clients.
- Endpoints para serviço externo de produtos.

Esse serviço foi desenvolvido usando o [Nest](https://github.com/nestjs/nest), [TypeScript](https://www.typescriptlang.org/) e o [Mongoose](https://mongoosejs.com/). Foi utilizado o framework de teste [Jest](https://jestjs.io/).

# Configurações de Ambiente

## Desenvolvimento

- Tenha instalado [Node](https://nodejs.org/en/download/) ou [Docker](https://docs.docker.com/install/) e [Docker-compose](https://docs.docker.com/compose/install/), dependendo de como irá executar o projeto.

- Crie um arquivo `.env` usando como exemplo o `.env-example`.

- Se quiser rodar com docker, somente rodar o comando `docker-compose up -d`. Vai ser criado um mongoDB, mongo-express e a API (pode demorar alguns minutos).

## Instalação

```bash
npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# test coverage
$ npm run test:cov
```

## Endpoints

### Auth

Esse endpoint serve para autenticar o `User` criado e retornar uma JWT para ser utilizada nos outros endpoints.
A autenticação foi feita pelo método `Bearer token`.


| método   | caminho                                |
|----------|----------------------------------------|
| `POST`   | `/auth/login`                          |

### Clients

Esses são os endpoints de CRUD do cliente e seus produtos favoritos (necessita JWT).
Os endpoints de produtos favoritos utilizam o `external_id` como IDs dentro do campo `product_ids`, que seria o ID que vem dos endpoints externos disponibilizados.

| método   | caminho                                |
|----------|----------------------------------------|
| `POST`   | `/clients`                             |
| `GET`    | `/clients`                             |
| `GET`    | `/clients/:clientID`                   |
| `PATCH`  | `/clients/:clientID`                   |
| `DELETE` | `/clients/:clientID`                   |
| `POST`   | `/clients/:clientID/favorite-products` |
| `GET`    | `/clients/:clientID/favorite-products` |
| `DELETE` | `/clients/:clientID/favorite-products` |

### Health

Esse endpoint serve para testar a saúde da API.

| método   | caminho                                |
|----------|----------------------------------------|
| `GET`    | `/health`                              |

### Products

Esses endpoints pegam os dados referentes a API externa de produtos disponibilizada.

| método   | caminho                                |
|----------|----------------------------------------|
| `GET`    | `/products`                            |
| `GET`    | `/products/:productID`                 |

### Users

Esses endpoints são para criação do usuário que utiliza a API. Com esse usuário é feita a autenticação no endpoint `/auth/login`.

| método   | caminho                                |
|----------|----------------------------------------|
| `POST`   | `/users`                               |

## Banco de dados

Foi utilizado o mongoDB com o framework mongoose para a criação do bando de dados para a API. Foram criadas três tabelas no mongoDB. A tabela `Users` que serve para criar o usuário para autenticação  e permissionamento (não foi feito mas já foi preparado) na API. A tabela `Client` para servir como tabela principal de clientes e suas informações. E a tabela `FavoriteProducts` que serve como uma tabela com relação `N - N` da tabela `Client`, tendo em cada documento, uma Foreign Key `client_id` apontando para a tabela `Client`. O campo `external_id` é o ID que vem da API externa dispobilizada de produtos.

Se você subiu o docker-compose é possível também acessar o `mongo-express` pela URL: `http://localhost:8081` utilizando a conta que voce inserir na env `ME_CONFIG_BASICAUTH_USERNAME` e `ME_CONFIG_BASICAUTH_PASSWORD`.

![](media/database-diagram.png)


## Trabalhos futuros

Devido ao tempo, não consegui lapidar a API do jeito que gostaria. Aqui estão alguns pontos a mais que gostaria de ter feito.

- Mais testes de integração e unitários.
- Permissionamento utilizando o campo `roles` da entidade `Users`.
- Adicionar sorting nos endpoints de GET.
- Melhorar algumas descrições do OpenAPI.

# Documentação

A documentação interna da API encontra-se na URL: `http://localhost:{PORT}/api`

[OpenAPI](http://localhost:3000/api)
